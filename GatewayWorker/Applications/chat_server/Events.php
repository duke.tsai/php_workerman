<?php
/**
 * This file is part of workerman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link http://www.workerman.net/
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * 用于检测业务代码死循环或者长时间阻塞等问题
 * 如果发现业务卡死，可以将下面declare打开（去掉//注释），并执行php start.php reload
 * 然后观察一段时间workerman.log看是否有process_timeout异常
 */
//declare(ticks=1);
use \GatewayWorker\Lib\Gateway;
use \GatewayWorker\Lib\Db;
/**
 * 主逻辑
 * 主要是处理 onConnect onMessage onClose 三个方法
 * onConnect 和 onClose 如果不需要可以不用实现并删除
 */
class Events
{

    public static $db;
    public static $acc_url;
    public static $zht_url;
    public static $tz_url;
    public static $domain_list;
    public static $lang;
    public static function onWorkerStart($worker)
    {
        include_once('Config/Domain.php');
        include_once('language/language.php');
        self::$domain_list = $domain_list;
        self::$lang = $local_code;
        self::$db = Db::instance('chat_tool');
    }

    /**
     * 当客户端连接时触发
     * 如果业务不需此回调可以删除onConnect
     * 
     * @param int $client_id 连接id
     */
    public static function onConnect($client_id) {
        $result_data['result'] = 0;
        //$result_data['data']['list'] = self::all_data(Gateway::getClientSessionsByGroup('all_group'));
        $result_data['data']['list'] = array();
        $result_data['data']['action'] = 'connect';
        Gateway::sendToClient($client_id, json_encode($result_data));
    }
    
   /**
    * 当客户端发来消息时触发
    * @param int $client_id 连接id
    * @param mixed $message 具体消息
    */
    public static function onMessage($client_id, $message){
        set_time_limit(0);
        
        $msg_decode_data = json_decode($message, true);
        //$msg_decode_data['zht_url'] = self::$domain_list['zht'];
        //$msg_decode_data['tz_url'] = self::$domain_list[$msg_decode_data['platform_code']]['tz'];
        $msg_decode_data['acc_url'] = self::$domain_list[$msg_decode_data['platform_code']]['acc'];

        switch($msg_decode_data['action']){
            
            case "login":
                self::login($client_id,$msg_decode_data);
                break;
            case "send_msg":
                self::send_msg($client_id,$msg_decode_data);
                break;
            case "get_msg":
                self::get_msg($client_id,$msg_decode_data);
                break;
            case "create_group":
                self::create_group($client_id,$msg_decode_data);
                break;
            case "hide_group":
                self::hide_group($client_id,$msg_decode_data);
                break;
            case "get_group":
                self::get_group($client_id,$msg_decode_data);
                break;
            case "join_room":
                self::join_room($client_id,$msg_decode_data);
                break;
            case "logout":
                self::logout($client_id,$msg_decode_data);
                break;
            case "keep_connect":
                self::keep_connect($client_id,$msg_decode_data);
                break;
        }
    }
   
   /**
    * 当用户断开连接时触发
    * @param int $client_id 连接id
    */
    public static function onClose($client_id) {
         
        $user_data = $_SESSION;
        if(!empty($user_data)){
            $result_data['result'] = 0;
            $result_data['data']['list'] = self::all_data(Gateway::getClientSessionsByGroup($user_data['group_id']));
            $result_data['data']['action'] = 'offline';
            $result_data['data']['group_id'] = $user_data['group_id'];
            //Gateway::sendToClient($client_id, json_encode($result_data));
            Gateway::sendToGroup($user_data['group_id'], json_encode($result_data));
        }
    }
    
    //建立群組
    private static function create_group($client_id, $data){
        self::check_in($client_id, $data, __FUNCTION__);
        
        if($data['user_id'] != '0'){
            $check = self::check_group($data);
        }else{
            if($data['to_user_type'] != '2'){
                $result_data['result'] = 1;
                $result_data['data']['action'] = 'create_group';
                $result_data['data']['message'] = '访客只能和客服私聊';
                Gateway::sendToClient($client_id, json_encode($result_data));
                return;
            }
        }
        if($data['user_id'] == $data['to_user_id']){
            $result_data['result'] = 1;
            $result_data['data']['action'] = 'create_group';
            $result_data['data']['message'] = '参数错误';
            Gateway::sendToClient($client_id, json_encode($result_data));
            return;
        }
        
        $client_session = Gateway::getSession($client_id);
        
        if(empty($check)){
            $group_id = $data['platform_code'].'_'.self::get_microtime();
            $group_array = array(
                                    'platform_code' => $data['platform_code'],
                                    'group_id' => $group_id,
                                    'chcr_id' => $data['room_id']
                                );
            $chcg_id = self::$db->insert('ch_chat_group')->cols($group_array)->query();
            
            if($chcg_id !== false){
                $group_user_array = array(
                                    'group_id' => $group_id,
                                    'user_type' => $data['user_type'],
                                    'user_id' => $data['user_id'],
                                    'chcr_id' => $data['room_id']
                                );
                
                if($data['user_id'] == '0' && $data['user_type'] == '3'){
                    $group_user_array['nick_name'] = '访客-'.str_pad(self::guest_count($data)[0]['count'] + 1, 5, '0', STR_PAD_LEFT);
                    
                    $session_data = array(
                        'user_type' => $data['user_type'],
                        'platform_code' => $data['platform_code'],
                        'user_id' => $data['user_id'],
                        'name' => $group_user_array['nick_name'],
                        'group_id' => $group_id,
                        'group_array' => array($group_id),
                        'room_id' => $data['room_id']
                    );
            
                    Gateway::setSession($client_id, $session_data);
                }else{
                    $client_session['group_array'][] = $group_id;
                    Gateway::setSession($client_id, $client_session);
                }
                
                self::add_group_user($group_user_array); 
                $to_group_user_array = array(
                                    'group_id' => $group_id,
                                    'user_type' => $data['to_user_type'],
                                    'user_id' => $data['to_user_id'],
                                    'chcr_id' => $data['room_id']
                                );
                self::add_group_user($to_group_user_array);          
            }
        }else{
            $group_id = $check[0]['group_id'];
        }
        
        $to_c_id = '';
        foreach(Gateway::getClientIdByUid($data['platform_code'].$data['to_user_id']) as $c_id){
            $c_session = Gateway::getSession($c_id);
            if($c_session['room_id'] == $data['room_id']){
                $c_session['group_array'][] = $group_id;
                $to_c_id = $c_id;
                $to_result_data = array(
                                        'group_id' => $group_id,
                                        'user_type' => $data['user_type'], 
                                        'user_id' => $data['user_id'], 
                                        'name' => $client_session['name']
                                    );
                Gateway::setSession($c_id, $c_session);
                Gateway::joinGroup($c_id, $group_id);
            }
        }
            
        $result_data['result'] = 0;
        $result_data['data']['list'] = array(
                                                'group_id' => $group_id,
                                                'user_type' => $data['to_user_type'], 
                                                'user_id' => $data['to_user_id'], 
                                                'name' => $data['to_user_name']
                                            );
        $result_data['data']['action'] = 'create_group';
        
        Gateway::sendToClient($client_id, json_encode($result_data));
        if(!empty($to_c_id)){
            $result_data['data']['list'] = $to_result_data;
            Gateway::sendToClient($to_c_id, json_encode($result_data));     
        }            
    }
    
    //加入房間
    private static function join_room($client_id, $data){
        $user_data = Gateway::getSession($client_id);
        
        if(!empty($user_data)){
            $group_array = $user_data['group_array'];
            foreach($group_array as $group_id){
                Gateway::leaveGroup($client_id, $group_id);
            }
            Gateway::setSession($client_id, array());
        }
        
        $group_id = 'all_group_'.$data['room_id'];
        $result_data['result'] = 0;
        $result_data['data']['list'] = self::all_data(Gateway::getClientSessionsByGroup($group_id));
        $result_data['data']['action'] = 'join_room';
        $result_data['data']['group_id'] = $group_id;
        Gateway::joinGroup($client_id, $group_id);
        Gateway::sendToClient($client_id, json_encode($result_data));       
    }
    
    //登出
    private static function logout($client_id, $data){
        self::check_in($client_id, $data, __FUNCTION__);
        
        $client_array = Gateway::getClientIdByUid($data['platform_code'].$data['user_id']);

        if(!in_array($client_id, $client_array)){
            $result_data['result'] = 1;
            $result_data['data']['action'] = 'logout';
            $result_data['data']['message'] = '参数错误';
            Gateway::sendToClient($client_id, json_encode($result_data));
            return;
        }

        foreach($client_array as $c_id){
            $user_data = Gateway::getSession($c_id);
            
            $group_array = $user_data['group_array'];
            foreach($group_array as $group_id){
                Gateway::leaveGroup($c_id, $group_id);
            }
            
            Gateway::setSession($c_id, array());
            Gateway::joinGroup($c_id, $user_data['group_id']);
            $result_data['result'] = 0;
            $result_data['data']['list'] = self::all_data(Gateway::getClientSessionsByGroup($user_data['group_id']));
            $result_data['data']['action'] = 'join_room';
            $result_data['data']['group_id'] = $user_data['group_id'];
            Gateway::sendToClient($c_id, json_encode($result_data)); 

            $result_data['data']['action'] = 'offline';
            Gateway::sendToGroup($user_data['group_id'], json_encode($result_data), array($c_id));
        }
    }
    
    //保持連線
    private static function keep_connect($client_id, $data){

        $result_data['result'] = 0;
        $result_data['data']['list'] = array();
        $result_data['data']['action'] = 'keep_connect';
        $result_data['data']['group_id'] = $data['group_id'];
        Gateway::sendToClient($client_id, json_encode($result_data));       
    }
    
    //隱藏群組
    private static function hide_group($client_id, $data){
        //self::check_in($client_id, $data, __FUNCTION__);
        
        self::update_group($data, array('status' => 1));
        $result_data['result'] = 0;
        $result_data['data']['list'] = array();
        $result_data['data']['action'] = 'hide_group';
        $result_data['data']['group_id'] = $data['group_id'];
        Gateway::sendToClient($client_id, json_encode($result_data));        
    }
    
    //取得用戶所有群組
    private static function get_group($client_id, $data){
        self::check_in($client_id, $data, __FUNCTION__);
        
        $group_array = array();
        $all_user = Gateway::getClientSessionsByGroup('all_group_'.$data['room_id']);
        $all_user_id = array_column($all_user, 'user_id');
        //$all_user_name = array_column($all_user, 'name');
        if(!empty($all_user_id)){    
            $result = self::$db->select('ch_chat_group_user.group_id,user_type,user_id,status,ch_chat_group_user.nick_name as name')
                                ->from('ch_chat_group_user')
                                ->join('ch_chat_group', 'ch_chat_group.group_id = ch_chat_group_user.group_id', 'left')
                                //->where("ch_chat_group_user.chcr_id=".$data['room_id']." and ch_chat_group_user.group_id in ((SELECT group_id FROM ch_chat_group_user where user_id = ".$data['user_id'].")) and user_id != ".$data['user_id']." and user_id in (".implode(',', $all_user_id).")")
                                ->where("ch_chat_group_user.chcr_id=".$data['room_id']." and ch_chat_group_user.group_id in ((SELECT group_id FROM ch_chat_group_user where user_id = ".$data['user_id'].")) and user_id != ".$data['user_id']." and ch_chat_group_user.group_id like '".$data['platform_code']."_%'")
                                ->query();
                                
            if(!empty($result)){
                $group_user_id = array_column($result, 'user_id');
                
                $jwt_array = array(
                                        'platform_code' => $data['platform_code'],
                                        'user_ids' => $group_user_id
                                    );
                $jwt = self::create_jwt($jwt_array);
                $post_data = array('data'=>$jwt);
                $user_data = self::curl($data['acc_url'].'/private/v1/users/get_nickname_by_user_id',$post_data);
                $user_name_array = $user_data['data']['list'][0];
                
                foreach($all_user_id as $user_id){
                    if($user_id == $data['user_id']){
                        foreach($result as $group){
                            if(empty($group['name'])){
                                //$index = array_search($user_id, $all_user_id);
                                //$group['name'] = $all_user_name[$index];
                                $group['name'] = $user_name_array[$group['user_id']];
                            }
                            if($group['status'] == '0'){
                                unset($group['status']);
                                $group_array[$user_id][] = $group;
                            }
                        }
                    }else{
                        $index = array_search($user_id, $group_user_id);
                        if($index === false){
                            $group_array[$user_id] = array();
                        }else{
                            
                            if(empty($result[$index]['name'])){
                                //$result[$index]['name'] = $all_user_name[array_search($user_id, $all_user_id)];
                                $result[$index]['name'] = $user_name_array[$user_id];
                            }
                            $group_array[$user_id][] = $result[$index];
                            unset($group_array[$user_id]['status']);
                        }
                    }
                }
            }
        }
        $result_data['result'] = 0;
        $result_data['data']['list'] = $group_array;
        $result_data['data']['action'] = 'get_group';
        Gateway::sendToClient($client_id, json_encode($result_data));  
    }
    
    
    //檢查群組是否存在
    private static function check_group($data){
        $result = self::$db->select('group_id')
                            ->from('ch_chat_group_user')
                            ->where('user_id in ('.$data['user_id'].','.$data['to_user_id'].') and chcr_id='.$data['room_id']. " and group_id like '".$data['platform_code']."_%'")
                            ->groupBy(array('group_id'))
                            ->having('COUNT(group_id) > 1')
                            ->query();
        return $result;
    }
    
    //查詢訪客數量
    private static function guest_count($data){
        $result = self::$db->select('count(*) count')
                            ->from('ch_chat_group_user')
                            ->join('ch_chat_group', 'ch_chat_group.group_id = ch_chat_group_user.group_id', 'left')
                            ->where("ch_chat_group_user.user_id = 0 and ch_chat_group_user.user_type = 3 and ch_chat_group.platform_code='".$data['platform_code']."'")
                            ->query();
        return $result;
    }
    
    //更新群組狀態
    private static function update_group($data, $update_data){
        $chcg_id = self::$db->update('ch_chat_group')->cols($update_data)->where("platform_code ='".$data['platform_code']."' and group_id ='".$data['group_id']."'")->query();
    }
    
    //新增群組用戶
    private static function add_group_user($data){
        $chcg_id = self::$db->insert('ch_chat_group_user')->cols($data)->query();
    }
    
    //儲存對話紀錄
    private static function add_msg($data){
        $insert_id = self::$db->insert('ch_group_msg')->cols($data)->query();
    }
    
    //取得對話紀錄
    private static function get_msg($client_id, $data){
        self::check_in($client_id, $data, __FUNCTION__);
        
        if(!empty(strstr($data['group_id'],'all_group'))){
            $result = self::$db->select('user_id, user_type, `type`, msg, img_url, create_time')
                            ->from('ch_group_msg')
                            ->where("group_id = '".$data['group_id']."' and chcr_id='".$data['room_id']."'")
                            ->query();
        }else{
            $result = self::$db->select('cgm.user_id, cgm.user_type, cgm.`type`, cgm.msg, cgm.img_url, cgm.create_time, ccgu.nick_name name')
                            ->from('ch_group_msg cgm')
                            ->join('ch_chat_group_user ccgu', 'ccgu.group_id = cgm.group_id', 'left')
                            ->where("cgm.group_id = '".$data['group_id']."' and cgm.chcr_id='".$data['room_id']."'")
                            ->query();
        }
  
        if(!empty($result)){
            $group_user_id = array_column($result, 'user_id');
            
            $jwt_array = array(
                                    'platform_code' => $data['platform_code'],
                                    'user_ids' => $group_user_id
                                );
            $jwt = self::create_jwt($jwt_array);
            $post_data = array('data'=>$jwt);
            $user_data = self::curl($data['acc_url'].'/private/v1/users/get_nickname_by_user_id',$post_data);
            $user_name_array = $user_data['data']['list'][0];
            
            foreach($result as $k => $msg){
                $result[$k]['msg'] = urldecode($msg['msg']);
                $result[$k]['name'] = empty($msg['name'])? $user_name_array[$msg['user_id']] : $msg['name'];
            }
        }else{
            $result = array();
        }
        $result_data['result'] = 0;
        $result_data['data']['list'] = $result;
        $result_data['data']['action'] = 'get_msg';
        $result_data['data']['group_id'] = $data['group_id'];
        Gateway::sendToClient($client_id, json_encode($result_data));
    }

    //註冊聊天室
    private static function login($client_id,$data){
        
        $post_data = array('data'=>$data['token']);
        $user_data = self::curl($data['acc_url'].'/chat/v1/users/get_chat_user',$post_data);
        
        if($user_data['result']=='0'){

            $group_id = 'all_group_'.$data['room_id'];
            $user = $user_data['data']['list'][0];
            
            $user_client = Gateway::getClientIdByUid($data['platform_code'].$user['userId']);
            
            foreach($user_client as $c_id){
                $user_session = Gateway::getSession($c_id);
                if(!empty($user_session) && ($user_session['group_id'] == $group_id || $c_id == $client_id )){
                    $result_data['result'] = 1;
                    $result_data['data']['action'] = 'login';
                    $result_data['data']['message'] = '已重复登入';
                    Gateway::sendToClient($client_id, json_encode($result_data));
                    return;
                }
            }
            
            $session_data = array(
                'user_type' => ($user['isCustomerService'] != '0') ? '1' : '2',
                'platform_code' => $data['platform_code'],
                'user_id' => $user['userId'],
                'name' => $user['nickName'],
                'group_id' => $group_id,
                'group_array' => array($group_id),
                'room_id' => $data['room_id']
            );
            
            Gateway::setSession($client_id, $session_data);
            Gateway::bindUid($client_id, $data['platform_code'].$user['userId']);
                
            $result_data['result'] = 0;
            $result_data['data']['list'] = self::all_data(Gateway::getClientSessionsByGroup($group_id));
            $result_data['data']['action'] = 'login';
            $result_data['data']['group_id'] = $group_id;
            Gateway::sendToClient($client_id, json_encode($result_data));
            
            $result_data['data']['action'] = 'online';
            Gateway::sendToGroup($group_id, json_encode($result_data), array($client_id));
        }else{
            $result_data['result'] = 1;
            $result_data['data']['action'] = 'login';
            $result_data['data']['error'] = $user_data['data']['error'];
            $result_data['data']['message'] = $user_data['data']['message'];
            Gateway::sendToClient($client_id, json_encode($result_data));
            //Gateway::sendToGroup('all_group_'.$data['room_id'], json_encode($result_data));
        }        
    }
    
    private static function all_data($data)
    {
        $result = array(
                        'member' => array(),
                        'member_count' => 0,
                        'staff' => array(),
                        'staff_count' => 0
                    );
                    
        if(!empty($data)){
            foreach($data as $user){
                if(!empty($user)){
                    unset($user['group_id']);
                    unset($user['group_array']);
                    unset($user['room_id']);
                    if($user['user_type'] == '1'){
                        $result['member'][] = $user;
                        $result['member_count'] += 1;
                    }else if($user['user_type'] == '2'){
                        $result['staff'][] = $user;
                        $result['staff_count'] += 1;
                    }
                }
            }
        }
        return $result;
    }
    
    private static function check_in($c_id, $data, $action)
    {
        $check = true;
        $c_session = Gateway::getSession($c_id);
        if(!empty($c_session)){
            /*
            if(!empty($data['group_id'])){
                if(!in_array($data['group_id'], $c_session['group_array'])){
                    $check = false;
                }
            }*/
            if(!empty($data['room_id']) && $data['room_id'] != $c_session['room_id']){
                $check = false;
            }
            if(!empty($data['user_id']) && $data['user_id'] != $c_session['user_id']){
                $check = false;
            }
            if(!empty($data['platform_code']) && $data['platform_code'] != $c_session['platform_code']){
                $check = false;
            }
            
            if(!$check){
                $result_data['result'] = 1;
                $result_data['data']['action'] = $action;
                $result_data['data']['message'] = '参数错误';
                Gateway::sendToClient($c_id, json_encode($result_data));
                exit;
            }
        }
    }
    
    //發送訊息
    private static function send_msg($client_id, $data)
    {
        $user_data = Gateway::getSession($client_id);
        if(!empty(strstr($data['group_id'],'all_group'))){
            if($data['user_id'] == '0'){
                $result_data['result'] = 1;
                $result_data['data']['action'] = 'send_msg';
                $result_data['data']['message'] = '访客无法发送讯息';
                Gateway::sendToClient($client_id, json_encode($result_data));
                return;
            }
            self::check_in($client_id, $data, __FUNCTION__);
        }else{
            $join_user = self::$db->select('user_id,chcr_id')
                                ->from('ch_chat_group_user')
                                ->where("chcr_id=".$data['room_id']." and group_id='".$data['group_id']."'")
                                ->query();
            $user_array = array_column($join_user, 'user_id');
            
            if(!in_array($data['user_id'], $user_array) || $user_data['user_id'] != $data['user_id']){
                $result_data['result'] = 1;
                $result_data['data']['action'] = 'send_msg';
                $result_data['data']['message'] = '参数错误';
                Gateway::sendToClient($client_id, json_encode($result_data));
                return;
            }else{
                $client_array = Gateway::getClientInfoByGroup($data['group_id']);
                
                foreach($join_user as $user){
                    foreach(Gateway::getClientIdByUid($data['platform_code'].$user['user_id']) as $c_id){
                        $c_session = Gateway::getSession($c_id);
                        if($user['chcr_id'] == $c_session['room_id']){
                            if(!array_key_exists($c_id,$client_array)){
                                Gateway::joinGroup($c_id, $data['group_id']);
                                $c_session['group_array'][] = $data['group_id'];
                                Gateway::setSession($c_id, $c_session);
                            }
                        }
                    }  
                }
            }
            $no_read = true;
        }
        
        $now = date('Y-m-d H:i:s');
        $msg_array = array(
                        'user_id' => $data['user_id'],
                        'user_type' => $user_data['user_type'],
                        'chcr_id' => $data['room_id'],
                        'group_id' => $data['group_id'],
                        'type' => $data['msg_type'],
                        'msg' => urlencode($data['msg']),
                        'img_url' => empty($data['img_url'])? '' : $data['img_url'],
                        'create_time' => $now
                    );
        self::add_msg($msg_array);
        
        $msg_array['msg'] = $data['msg'];
        $msg_array['name'] = $user_data['name'];
        if(isset($no_read) && $no_read){
            $msg_array['no_read'] = 1;
        }
        unset($msg_array['chcr_id']);
        $result_data['result'] = 0;
        $result_data['data']['list'] = $msg_array;
        $result_data['data']['action'] = 'send_msg';
        $result_data['data']['group_id'] = $data['group_id'];
        Gateway::sendToGroup($data['group_id'], json_encode($result_data));
    }

    //產生microtime
    private static function get_microtime()
    {
        $time = explode(' ',microtime());
        $explode_time = explode('.',$time[0]);
        $microtime = $time[1].$explode_time[1];
        return $microtime;
    }

    //獲取IP
    private static function get_ip()
    {
        $http_array = array(
                            'HTTP_CLIENT_IP',
                            'HTTP_X_FORWARDED_FOR',
                            'HTTP_X_FORWARDED',
                            'HTTP_X_CLUSTER_CLIENT_IP',
                            'HTTP_FORWARDED_FOR',
                            'HTTP_FORWARDED',
                            'REMOTE_ADDR'
                        );
                        
        foreach ($http_array as $key) {
                if (array_key_exists($key, $_SERVER)) {
                    foreach (explode(',', $_SERVER[$key]) as $ip) {
                        $ip = trim($ip);
                        return $ip;
                    }
                }
        }
        return null;
    }

    //curl 
    private static function curl($url, $param='')
    {
        $start_time=date('Y-m-d H:i:s');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if($param !=''){
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($param));
        }
        $ret = json_decode(curl_exec($ch),1);
        curl_close($ch);
        return $ret;
    }

    //產生jwt
    private static function create_jwt($params)
    {
        $jwt = urlencode(base64_encode(urlencode(json_encode($params))));
        return $jwt;
    }

}
