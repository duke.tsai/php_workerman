#!/usr/bin/php
<?php
use Workerman\Worker;
// use Workerman\MySQL;
require_once __DIR__ . '/../Workerman/Autoloader.php';
require_once __DIR__ .'/../Workerman/mysql/src/Connection.php';

// 创建一个Worker监听2346端口，使用websocket协议通讯
$worker = new Worker("websocket://0.0.0.0:2346");


$worker->count = 1;
// worker进程启动后创建一个text Worker以便打开一个内部通讯端口
$worker->onWorkerStart = function($worker)
{
	// var_dump($worker);
	 // echo "worker->id={$worker->id}\n";
	global $db;
    $dbhost = '192.168.99.200';
	$dbuser = 'root978';
	$dbpass = 'ReQDM8S8pU';
	$dbname = 'websocket';
    $db = new Workerman\MySQL\Connection($dbhost,'3306', $dbuser, $dbpass, $dbname);
};

$worker->onConnect = function($connection)
{

};

// 新增加一个属性，用来保存uid到connection的映射
$worker->uidConnections = array();
// 当有客户端发来消息时执行的回调函数
$worker->onMessage = function($connection, $data)
{
    global $worker;
    echo $data.'\r\n';
    $data = json_decode($data,true);
    // 判断当前客户端是否已经验证,既是否设置了uid
    if($data['action'] == 'register')
    {
    	if(!isset($connection->uid))
	    {
			// 没验证的话把第一个包当做uid（这里为了方便演示，没做真正的验证）
			$connection->uid = $data['name'];
			/* 保存uid到connection的映射，这样可以方便的通过uid查找connection，
			* 实现针对特定uid推送数据
			*/
			$worker->uidConnections[$connection->uid] = $connection;
			$json_data['action'] = "broadcast";
			$json_data['content'] = $data['name']."註冊成功";
			sendMessageByUid($data['name'],json_encode($json_data));
			show_user_list();
	       	return;
	    }
    }
    else if($data['action'] == 'message')
    {
    	sendMessageByUid($data['toName'], json_encode($data));
    }
    
};

// 当有客户端连接断开时
$worker->onClose = function($connection)
{
    global $worker;
    if(isset($connection->uid))
    {
        // 连接断开时删除映射
        unset($worker->uidConnections[$connection->uid]);
        show_user_list();
    }
};

function show_user_list()
{
	global $worker;
	$users['action'] = 'system';
	// $list = array_keys($worker->uidConnections);
	// $users['data'] = json_encode($list);
	foreach($worker->uidConnections as $key => $value)
	{
		$users['data'][] = $key;
	}
	$users = json_encode($users);
	broadcast($users);
}

// 向所有验证的用户推送数据
function broadcast($message)
{
   global $worker;
   foreach($worker->uidConnections as $connection)
   {
        $connection->send($message);
   }
}

// 针对uid推送数据
function sendMessageByUid($uid, $message)
{
    global $worker;
    if(isset($worker->uidConnections[$uid]))
    {
    	global $db;
	    // 执行SQL
	    $db->query("INSERT INTO `message_log` (content) VALUES ('{$message}')");
        $connection = $worker->uidConnections[$uid];
        $connection->send($message);
        return true;
    }
    return false;
}

// 运行所有的worker
Worker::runAll();