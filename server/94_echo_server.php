#!/usr/bin/php
<?php
use Workerman\Worker;
require_once __DIR__ . '/../Workerman/Autoloader.php';

// 创建一个Worker监听2346端口，使用websocket协议通讯
$ws_worker = new Worker("websocket://192.168.99.94:2346");

// 启动4个进程对外提供服务
$ws_worker->count = 4;

// 当收到客户端发来的数据后返回hello $data给客户端
$ws_worker->onMessage = function($connection, $data)
{
    // 向客户端发送hello $data
    echo $data;
    //$connection->send($data);
    
    //var_dump($connection);

    // ob_start();
    // var_dump($connection);
    // $c = ob_get_contents();
    // ob_end_clean();


    $connection->send($data);
};

// 运行worker
Worker::runAll();