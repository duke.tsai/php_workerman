CREATE DATABASE chat_tool CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE `ch_chat_room` (
  `chcr_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `name` varchar(100) NOT NULL COMMENT '房間名稱',
  `password` varchar(100) NOT NULL COMMENT '房間密碼',
  `platform_code` varchar(45) NOT NULL COMMENT '廳',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:開啟  1.:關閉',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立時間',
  `update_time` timestamp NULL COMMENT '更新時間',
  PRIMARY KEY (`chcr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO ch_chat_room(chcr_id, name, password,platform_code)
VALUES(1,'長樂彩貴賓房', 'MTIzNHF3ZXI=', 'FFF002'),(2,'長樂彩遊戲討論房', 'MTIzNHF3ZXI=', 'FFF002');

CREATE TABLE `ch_chat_notify` (
  `chcn_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `chcr_id` int(11) NOT NULL COMMENT '關聯房間ID',
  `content` varchar(255) NOT NULL COMMENT '公告內容',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:開啟  1.:關閉',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立時間',
  `update_time` timestamp NULL COMMENT '更新時間',
  PRIMARY KEY (`chcn_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO ch_chat_notify(chcr_id,content)
VALUES(1,'迎来到長樂彩票聊天室，任何疑難雜症都能在這裡詢問，祝你玩的愉快！'),
(1,'現在就前往長樂彩<a>www.changlecai.com</a>'),
(2,'迎来到長樂彩票聊天室，任何遊戲問題都能在這裡討論，祝你玩的愉快！'),
(2,'現在就前往長樂彩<a>www.changlecai.com</a>');

CREATE TABLE `ch_group_msg` (
  `chgm_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `chcr_id` int(11) NOT NULL COMMENT '關聯房間ID',
  `user_id` int(11) NOT NULL COMMENT '用戶ID',
  `user_type` tinyint(4) NOT NULL COMMENT '1:會員  2:客服  3:訪客  4:系統',
  `group_id` varchar(45) NOT NULL COMMENT '群組編碼',
  `type` tinyint(4) NOT NULL COMMENT '1:文字  2:圖片',
  `msg` varchar(4000) NOT NULL COMMENT '訊息',
  `img_url` varchar(200) DEFAULT NULL COMMENT '圖片URL',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立時間',
  PRIMARY KEY (`chgm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `ch_chat_group` (
  `chcg_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `platform_code` varchar(45) NOT NULL COMMENT '廳',
  `chcr_id` int(11) NOT NULL COMMENT '關聯房間ID',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:開啟  1:關閉',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立時間',
  `group_id` varchar(45) NOT NULL COMMENT '群組編號',
  PRIMARY KEY (`chcg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `ch_chat_group_user` (
  `chcgu_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號',
  `chcr_id` int(11) NOT NULL COMMENT '關聯房間ID',
  `group_id` varchar(45) NOT NULL COMMENT '群組編號',
  `user_type` tinyint(4) NOT NULL COMMENT '1:會員  2:客服  3:訪客',
  `user_id` int(11) NOT NULL COMMENT '使用者ID',
  `nick_name` varchar(45) NULL COMMENT '暱稱',
  `join_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '加入時間',
  PRIMARY KEY (`chcgu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


